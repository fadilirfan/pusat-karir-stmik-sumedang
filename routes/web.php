<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Content Management System
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
route::resource('users', 'UserController');
route::resource('career_guidance_users', 'CareerGuidanceUserController')->middleware('cekadmin');
route::resource('career_guidances', 'CareerGuidanceController')->middleware('cekadmin');
route::resource('career_guidance_alumnis', 'CareerGuidanceAlumniController');
route::resource('career_guidance_schedules', 'CareerGuidanceScheduleController')->middleware('cekadmin');
route::resource('job_vacancies', 'JobVacancyController')->middleware('cekadmin');
route::resource('job_vacancy_categories', 'JobVacancyCategoryController')->middleware('cekadmin');
route::resource('internships', 'InternshipController')->middleware('cekadmin');
route::resource('internship_categories', 'InternshipCategoryController')->middleware('cekadmin');
route::resource('events', 'EventController')->middleware('cekadmin');
route::resource('event_categories', 'EventCategoryController')->middleware('cekadmin');
route::resource('trainings', 'TrainingController')->middleware('cekadmin');
route::resource('training_categories', 'TrainingCategoryController')->middleware('cekadmin');
Route::put('/change_password/{id}', 'UserController@changePassword')->name('change_password');
Route::put('/close_career_guidance/{id}', 'CareerGuidanceController@closeCareerGuidance')->name('close_career_guidance');
Route::put('/close_career_guidance_alumni/{id}', 'CareerGuidanceAlumniController@closeCareerGuidance')->name('close_career_guidance_alumni');

//User Views
Route::resource('bimbingan-karir', 'BimbinganKarirController');
Route::resource('lowongan-kerja', 'LowonganKerjaController');
Route::resource('magang', 'MagangController');
Route::resource('acara', 'AcaraController');
Route::resource('pelatihan', 'PelatihanController');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
