<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CareerGuidanceUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('career_guidance_users')->insert([
            'nama_lengkap' => 'Muhamad Iqbal Rivaldi',
            'bidang_ilmu' => 'Psikolog'
        ]);
    }
}
