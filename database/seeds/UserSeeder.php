<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserLevelSeeder::class);
        DB::table('users')->insert([
            'username' => 'administrator',
            'fullname' => 'Administrator',
            'email' => 'admin@gmail.com',
            'gender' => 'Laki-laki',
            'user_level_id' => '1',
            'password' => Hash::make('password'),
        ]);
    }
}
