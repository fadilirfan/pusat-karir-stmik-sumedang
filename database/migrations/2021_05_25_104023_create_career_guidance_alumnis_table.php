<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCareerGuidanceAlumnisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_guidance_alumnis', function (Blueprint $table) {
            $table->id();
            $table->enum('program_studi', ['Teknik Informatika', 'Sistem Informasi', 'Manajemen Informasi']);
            $table->bigInteger('career_guidance_user_id')->unsigned();
            $table->bigInteger('author_id')->unsigned()->nullable();
            $table->bigInteger('updator_id')->unsigned()->nullable();
            $table->string('nama_lengkap');
            $table->enum('jenis_kelamin', ['Pria', 'Wanita']);
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('telepon');
            $table->string('email')->unique()->nullable();
            $table->text('area_permasalahan')->nullable();
            $table->enum('sesi', ['individual', 'kelompok', 'keduanya'])->nullable();
            $table->string('alasan_sesi')->nullable();
            $table->string('keterangan_diri')->nullable();
            $table->string('cv')->nullable();
            $table->string('jadwal');
            $table->enum('status', ['Open', 'Close'])->default('Open');
            $table->timestamps();
            $table->foreign('career_guidance_user_id')->references('id')->on('career_guidance_users');
            $table->foreign('author_id')->references('id')->on('users');
            $table->foreign('updator_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_guidance_alumnis');
    }
}
