<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobVacancyCategory extends Model
{
    protected $fillable = [
        'nama', 'catatan'
    ];

}
