<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerGuidanceAlumni extends Model
{
    protected $fillable = [
        'program_studi', 'career_guidance_user_id', 'nama_lengkap', 'jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'telepon', 'email', 'area_permasalahan', 'sesi', 'alasan_sesi', 
        'keterangan_diri', 'jadwal', 'cv', 'status', 'catatan', 'author_id', 'updator_id'
    ];

    public function pembimbing(){
        return $this->belongsTo(CareerGuidanceUser::class, 'career_guidance_user_id');
    }
}
