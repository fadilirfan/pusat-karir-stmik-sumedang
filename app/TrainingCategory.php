<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingCategory extends Model
{
    protected $fillable = [
        'nama', 'catatan'
    ];

}
