<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CareerGuidance;
use App\CareerGuidanceUser;
use App\CareerGuidanceSchedule;
use App\SelfDescription;
use Illuminate\Support\Facades\Storage;

class BimbinganKarirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data['career_guidance_users'] = CareerGuidanceUser::all();
        $data['career_guidance_schedules'] = CareerGuidanceSchedule::all();
        $data['self_descriptions'] = SelfDescription::all();
        return view('bimbingan_karir', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nim' => 'required|max:255',
            'program_studi' => 'required',
            'career_guidance_user_id' => 'required',
            'nama_lengkap' => 'required|regex:/^[\pL\s\-]+$/u',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'telepon' => 'required|unique:career_guidances|digits_between:9,16|numeric',
            'email' => 'required|unique:career_guidances',
            'area_permasalahan' => 'required',
            'sesi' => 'required',
            'alasan_sesi' => 'required',
            'keterangan_diri' => 'required',
            'jadwal' => 'required',
            'cv' => 'required|mimes:pdf,docx,pptx|max:2048'
        ]);

        $path = ($request->cv)
        ? $request->file('cv')->store("/public/input/bimbingan_karir/cv")
        : null;

        if(isset($_POST['keterangan_diri'])){
            if (is_array($_POST['keterangan_diri'])) {
                $value = implode(";",$_POST['keterangan_diri']);
                $keterangan_diri = $value;
              } else {
                $value = $_POST['keterangan_diri'];
                $keterangan_diri = $value;
           }
       }

        if(isset($_POST['jadwal'])){
            if (is_array($_POST['jadwal'])) {
                $value = implode(";",$_POST['jadwal']);
                $jadwal = $value;
              } else {
                $value = $_POST['jadwal'];
                $jadwal = $value;
           }
       }

        $bimbingan_karir = new CareerGuidance([
            'nim' => $request->get('nim'),
            'program_studi' => $request->get('program_studi'),
            'career_guidance_user_id' => $request->get('career_guidance_user_id'),
            'nama_lengkap' => $request->get('nama_lengkap'),
            'jenis_kelamin' => $request->get('jenis_kelamin'),
            'tempat_lahir' => $request->get('tempat_lahir'),
            'tanggal_lahir' => $request->get('tanggal_lahir'),
            'telepon' => $request->get('telepon'),
            'email' => $request->get('email'),
            'area_permasalahan' => $request->get('area_permasalahan'),
            'sesi' => $request->get('sesi'),
            'alasan_sesi' => $request->get('alasan_sesi'),
            'keterangan_diri' => $keterangan_diri,
            'jadwal' => $jadwal,
            'cv' => $path
        ]);

        $bimbingan_karir->save();
        return redirect('/bimbingan-karir')->with('success', 'Bimbingan karir berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
