<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Training;
use App\TrainingCategory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class TrainingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['trainings'] = Training::all();
        return view('back.training.index', $data);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['training_categories'] = TrainingCategory::all();
        return view('back.training.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'thumbnail' => 'required|mimes:jpg,jpeg,png,gif',
            'deskripsi' => 'required',
            'category_id' => 'required'
        ]);

        $path = ($request->thumbnail)
        ? $request->file('thumbnail')->store("/public/input/pelatihan")
        : null;

        $pelatihan = new Training([
            'author_id' => Auth::user()->id,
            'judul' => $request->get('judul'),
            'slug' => Str::slug($request->get('judul')),
            'thumbnail' => $path,
            'deskripsi' => $request->get('deskripsi'),
            'category_id' => $request->get('category_id')
        ]);

        $pelatihan->save();
        return redirect('/trainings')->with('success', 'Pelatihan berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['detail'] = Training::findOrFail($id);
        $data['training_categories'] = TrainingCategory::all();
        return view('back.training.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'thumbnail' => 'mimes:jpg,jpeg,png,gif',
            'deskripsi' => 'required',
            'category_id' => 'required'
        ]);
        
        $pelatihan = Training::findOrFail($id);
        $path = ($request->thumbnail)
        ? $request->file('thumbnail')->store("/public/input/pelatihan")
        : null;


        if ($request->thumbnail) {
            Storage::delete($pelatihan->thumbnail);
            $pelatihan->thumbnail = $path;
        }

        $pelatihan->judul =  $request->get('judul');
        $pelatihan->slug = Str::slug($request->get('slug'));
        $pelatihan->deskripsi = $request->get('deskripsi');
        $pelatihan->category_id = $request->get('category_id');
        $pelatihan->save();

        return redirect('/trainings')->with('success', 'Pelatihan berhasil diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pelatihan = Training::findOrFail($id);
        if ($pelatihan->thumbnail) {
            Storage::delete($pelatihan->thumbnail);
        }
        if ($pelatihan->delete()) {
            return redirect('/trainings')->with('success', 'Pelatihan berhasil dihapus!');
        }else{
            return redirect('/trainings')->with('error', 'Pelatihan gagal dihapus!');
        }
    }
}
