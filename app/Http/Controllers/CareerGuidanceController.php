<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CareerGuidance;
use App\CareerGuidanceUser;
use App\CareerGuidanceSchedule;
use App\SelfDescription;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class CareerGuidanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {   
        if (Auth::user()->user_level_id == 1) {
            $data['career_guidances'] = CareerGuidance::all();
        }else{
            $data['career_guidances'] = CareerGuidance::where('author_id', '=', Auth::user()->id);
        }
        return view('back.career_guidances.index', $data);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['career_guidance_users'] = CareerGuidanceUser::all();
        $data['career_guidance_schedules'] = CareerGuidanceSchedule::all();
        $data['self_descriptions'] = SelfDescription::all();
        return view('back.career_guidances.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nim' => 'required|max:255',
            'program_studi' => 'required',
            'career_guidance_user_id' => 'required',
            'nama_lengkap' => 'required|regex:/^[\pL\s\-]+$/u',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'telepon' => 'required|unique:career_guidances|digits_between:9,16|numeric',
            'email' => 'required|unique:career_guidances',
            'area_permasalahan' => 'required',
            'sesi' => 'required',
            'alasan_sesi' => 'required',
            'keterangan_diri' => 'required',
            'jadwal' => 'required',
            'cv' => 'required|mimes:pdf,docx,pptx|max:2048'
        ]);

        $path = ($request->cv)
        ? $request->file('cv')->store("/public/input/bimbingan_karir/cv")
        : null;

        if(isset($_POST['keterangan_diri'])){
            if (is_array($_POST['keterangan_diri'])) {
                $value = implode(";",$_POST['keterangan_diri']);
                $keterangan_diri = $value;
              } else {
                $value = $_POST['keterangan_diri'];
                $keterangan_diri = $value;
           }
       }

        if(isset($_POST['jadwal'])){
            if (is_array($_POST['jadwal'])) {
                $value = implode(";",$_POST['jadwal']);
                $jadwal = $value;
              } else {
                $value = $_POST['jadwal'];
                $jadwal = $value;
           }
       }

        $bimbingan_karir = new CareerGuidance([
            'nim' => $request->get('nim'),
            'program_studi' => $request->get('program_studi'),
            'career_guidance_user_id' => $request->get('career_guidance_user_id'),
            'nama_lengkap' => $request->get('nama_lengkap'),
            'jenis_kelamin' => $request->get('jenis_kelamin'),
            'tempat_lahir' => $request->get('tempat_lahir'),
            'tanggal_lahir' => $request->get('tanggal_lahir'),
            'telepon' => $request->get('telepon'),
            'email' => $request->get('email'),
            'area_permasalahan' => $request->get('area_permasalahan'),
            'sesi' => $request->get('sesi'),
            'alasan_sesi' => $request->get('alasan_sesi'),
            'keterangan_diri' => $keterangan_diri,
            'jadwal' => $jadwal,
            'cv' => $path,
            'author_id' => Auth::user()->id
        ]);

        $bimbingan_karir->save();
        return redirect('/career_guidances')->with('success', 'Bimbingan karir berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['detail'] = CareerGuidance::findOrFail($id);
        $ex_self_desc = explode(";",$data['detail']->keterangan_diri);
        $data['keterangan_diri'] = SelfDescription::whereIn('id',$ex_self_desc)->get();
        $ex_jadwal = explode(";",$data['detail']->jadwal);
        $data['jadwal'] = CareerGuidanceSchedule::whereIn('id',$ex_jadwal)->get();
        return view('back.career_guidances.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['detail'] = CareerGuidance::findOrFail($id);
        if ($data['detail']->status == 'Close') {
            return abort(401);die;
        }
        $data['ex_self_desc'] = explode(";",$data['detail']->keterangan_diri);
        $data['ex_jadwal'] = explode(";",$data['detail']->jadwal);
        $data['career_guidance_users'] = CareerGuidanceUser::all();
        $data['career_guidance_schedules'] = CareerGuidanceSchedule::all();
        $data['self_descriptions'] = SelfDescription::all();
        return view('back.career_guidances.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nim' => 'required|max:255',
            'program_studi' => 'required',
            'career_guidance_user_id' => 'required',
            'nama_lengkap' => 'required|regex:/^[\pL\s\-]+$/u',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'telepon' => 'required|digits_between:9,16|numeric|unique:career_guidances,telepon,'.$id,
            'email' => 'required|unique:career_guidances,email,'.$id,
            'area_permasalahan' => 'required',
            'sesi' => 'required',
            'alasan_sesi' => 'required',
            'keterangan_diri' => 'required',
            'jadwal' => 'required',
            'updator_id' => Auth::user()->id
        ]);
        
        $bimbingan = CareerGuidance::findOrFail($id);

        if ($request->cv) {
            $path = ($request->cv)
            ? $request->file('cv')->store("/public/input/bimbingan_karir/cv")
            : null;
            Storage::delete($bimbingan->cv);
            $bimbingan->cv = $path;
        }

        if(isset($_POST['keterangan_diri'])){
            if (is_array($_POST['keterangan_diri'])) {
                $value = implode(";",$_POST['keterangan_diri']);
                $keterangan_diri = $value;
              } else {
                $value = $_POST['keterangan_diri'];
                $keterangan_diri = $value;
           }
       }

        if(isset($_POST['jadwal'])){
            if (is_array($_POST['jadwal'])) {
                $value = implode(";",$_POST['jadwal']);
                $jadwal = $value;
              } else {
                $value = $_POST['jadwal'];
                $jadwal = $value;
           }
       }
        
        $bimbingan->nim =  $request->get('nim');
        $bimbingan->program_studi = $request->get('program_studi');
        $bimbingan->career_guidance_user_id = $request->get('career_guidance_user_id');
        $bimbingan->nama_lengkap = $request->get('nama_lengkap');
        $bimbingan->jenis_kelamin = $request->get('jenis_kelamin');
        $bimbingan->tempat_lahir = $request->get('tempat_lahir');
        $bimbingan->tanggal_lahir = $request->get('tanggal_lahir');
        $bimbingan->telepon = $request->get('telepon');
        $bimbingan->email = $request->get('email');
        $bimbingan->area_permasalahan = $request->get('area_permasalahan');
        $bimbingan->sesi = $request->get('sesi');
        $bimbingan->alasan_sesi = $request->get('alasan_sesi');
        $bimbingan->keterangan_diri = $keterangan_diri;
        $bimbingan->jadwal = $jadwal;
        $bimbingan->save();

        return redirect('/career_guidances')->with('success', 'Bimbingan berhasil diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bimbingan = CareerGuidance::findOrFail($id);
        if ($bimbingan->status == 'Close') {
            return abort(401);die;
        }
        if (Storage::exists($bimbingan->cv)) {
            Storage::delete($bimbingan->cv);
        }
        if ($bimbingan->delete()) {
            return redirect('/career_guidances')->with('success', 'Bimbingan berhasil dihapus!');
        }else{
            return redirect('/career_guidances')->with('error', 'Bimbingan gagal dihapus!');
        }
    }

    public function closeCareerGuidance(Request $request, $id){
        
        $career = CareerGuidance::findOrFail($id);

        if ($request->get('catatan')) {
            $request->validate([
                'catatan' => 'min:5|max:255'
            ]);
            $career->catatan = $request->get('catatan');
        }

        $career->status = 'Close';


        if ($career->save()) {
            return redirect(route('career_guidances.show', $id))->with('success', 'Status bimbingan berhasil ditutup!');
        }else{
            return redirect(route('career_guidances.show', $id))->with('error', 'Status bimbingan gagal ditutup!');
        }
    }
}
