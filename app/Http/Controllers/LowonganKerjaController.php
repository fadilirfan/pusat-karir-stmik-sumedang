<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JobVacancy;
use DataTables;
use Illuminate\Support\Carbon;

class LowonganKerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $data['job_vacancies'] = JobVacancy::paginate(6);
        // return view('lowongan_kerja.index', $data);
        Carbon::setLocale('id');
        if ($request->ajax()) {
            $data = JobVacancy::select('*');
            return Datatables::of($data)
            ->addIndexColumn()
                    ->editColumn('judul', function ($row) {
                        $judul = '<a href="'.url('lowongan-kerja/'.$row->slug).'">'.$row->judul.'</a>';
                            return $judul;
                    })
                    ->rawColumns(['judul'])
                    ->editColumn('created_at', function ($request) {
                        return date("F j, Y", strtotime($request->created_at));
                    })
                    ->make(true);
        }
        return view('lowongan_kerja.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data['detail'] = JobVacancy::where('slug',$slug)->first();
        return view('lowongan_kerja.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
