<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JobVacancy;
use App\JobVacancyCategory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class JobVacancyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['job_vacancies'] = JobVacancy::all();
        return view('back.job_vacancies.index', $data);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['job_vacancy_categories'] = JobVacancyCategory::all();
        return view('back.job_vacancies.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'thumbnail' => 'required|mimes:jpg,jpeg,png,gif',
            'deskripsi' => 'required',
            'category_id' => 'required',
            'status' => 'required'
        ]);

        $path = ($request->thumbnail)
        ? $request->file('thumbnail')->store("/public/input/lowongan_kerja")
        : null;

        $lowongan_kerja = new JobVacancy([
            'author_id' => Auth::user()->id,
            'judul' => $request->get('judul'),
            'slug' => Str::slug($request->get('judul')),
            'thumbnail' => $path,
            'deskripsi' => $request->get('deskripsi'),
            'category_id' => $request->get('category_id'),
            'status' => $request->get('status'),
        ]);

        $lowongan_kerja->save();
        return redirect('/job_vacancies')->with('success', 'Lowongan Kerja berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['detail'] = JobVacancy::findOrFail($id);
        $data['job_vacancy_categories'] = JobVacancyCategory::all();
        return view('back.job_vacancies.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'thumbnail' => 'mimes:jpg,jpeg,png,gif',
            'deskripsi' => 'required',
            'category_id' => 'required',
            'status' => 'required'
        ]);
        
        $lowongan_kerja = JobVacancy::findOrFail($id);
        $path = ($request->thumbnail)
        ? $request->file('thumbnail')->store("/public/input/lowongan_kerja")
        : null;


        if ($request->thumbnail) {
            Storage::delete($lowongan_kerja->thumbnail);
            $lowongan_kerja->thumbnail = $path;
        }

        $lowongan_kerja->judul =  $request->get('judul');
        $lowongan_kerja->slug = Str::slug($request->get('slug'));
        $lowongan_kerja->deskripsi = $request->get('deskripsi');
        $lowongan_kerja->category_id = $request->get('category_id');
        $lowongan_kerja->status = $request->get('status');
        $lowongan_kerja->save();

        return redirect('/job_vacancies')->with('success', 'Lowongan Kerja berhasil diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lowongan_kerja = JobVacancy::findOrFail($id);
        if ($lowongan_kerja->thumbnail) {
            Storage::delete($lowongan_kerja->thumbnail);
        }
        if ($lowongan_kerja->delete()) {
            return redirect('/job_vacancies')->with('success', 'Lowongan Kerja berhasil dihapus!');
        }else{
            return redirect('/job_vacancies')->with('error', 'Lowongan Kerja gagal dihapus!');
        }
    }
}
