<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InternshipCategory;
use Illuminate\Support\Facades\Storage;

class InternshipCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['internship_categories'] = InternshipCategory::all();
        return view('back.internship_categories.index', $data);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.internship_categories.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'catatan' => 'required'
        ]);

        $kategori_lowongan_kerja = new InternshipCategory([
            'nama' => $request->get('nama'),
            'catatan' => $request->get('catatan'),
        ]);

        $kategori_lowongan_kerja->save();
        return redirect('/internship_categories')->with('success', 'Kategori berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['job_vacancy_category'] = InternshipCategory::findOrFail($id);
        return view('back.internship_categories.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'catatan' => 'required',
        ]);
        
        $kategori_lowongan_kerja = InternshipCategory::findOrFail($id);

        $kategori_lowongan_kerja->nama =  $request->get('nama');
        $kategori_lowongan_kerja->catatan = $request->get('catatan');
        $kategori_lowongan_kerja->save();

        return redirect('/internship_categories')->with('success', 'Kategori berhasil diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori_lowongan_kerja = InternshipCategory::findOrFail($id);
        if ($kategori_lowongan_kerja->delete()) {
            return redirect('/internship_categories')->with('success', 'Kategori berhasil dihapus!');
        }else{
            return redirect('/internship_categories')->with('error', 'Kategori gagal dihapus!');
        }
    }
}
