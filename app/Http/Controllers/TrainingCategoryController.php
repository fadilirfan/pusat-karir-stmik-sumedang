<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TrainingCategory;
use Illuminate\Support\Facades\Storage;

class TrainingCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['training_categories'] = TrainingCategory::all();
        return view('back.training_categories.index', $data);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.training_categories.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'catatan' => 'required'
        ]);

        $kategori_lowongan_kerja = new TrainingCategory([
            'nama' => $request->get('nama'),
            'catatan' => $request->get('catatan'),
        ]);

        $kategori_lowongan_kerja->save();
        return redirect('/training_categories')->with('success', 'Kategori berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['training_category'] = TrainingCategory::findOrFail($id);
        return view('back.training_categories.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'catatan' => 'required',
        ]);
        
        $kategori_lowongan_kerja = TrainingCategory::findOrFail($id);

        $kategori_lowongan_kerja->nama =  $request->get('nama');
        $kategori_lowongan_kerja->catatan = $request->get('catatan');
        $kategori_lowongan_kerja->save();

        return redirect('/training_categories')->with('success', 'Kategori berhasil diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori_lowongan_kerja = TrainingCategory::findOrFail($id);
        if ($kategori_lowongan_kerja->delete()) {
            return redirect('/training_categories')->with('success', 'Kategori berhasil dihapus!');
        }else{
            return redirect('/training_categories')->with('error', 'Kategori gagal dihapus!');
        }
    }
}
