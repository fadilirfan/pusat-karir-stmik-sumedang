<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobVacancy extends Model
{
    protected $fillable = [
        'author_id', 'category_id', 'judul', 'thumbnail', 'deskripsi', 'slug', 'status'
    ];

    public function category(){
        return $this->belongsTo(JobVacancyCategory::class, 'category_id');
    }

    public function author(){
        return $this->belongsTo(User::class, 'author_id');
    }
}

