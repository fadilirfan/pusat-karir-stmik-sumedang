@extends('layouts.app')
@section('title')
    Bimbingan Karir
@endsection
@section('bg-image')
 style="background: url('assets/front/media/bimbingan-karir.jpeg') no-repeat;width:100%;background-size:cover;object-fit:cover"
@endsection
@section('content')
<section class="padding-100-0 position-relative">
    <div class="body_overlay_ono"></div>
    <div class="container">
        <div class="col-md-12 help-center-header ">
            <h2 class="help-center-title">Bimbingan Karir</h2>
            <p>Selamat datang di Pusat Layanan Bimbingan Karir Universitas Padjadaran. Layanan ini akan membantu anda dalam mengeksplorasi, memilih, dan membuat keputusan karir yang sesuai untuk anda.</p>
            <h2>Beberapa hal yang perlu diperhatikan dalam proses bimbingan karir</h2>
            <ol>
                <li>Bimbingan karir ini difasilitasi oleh Pembimbing Karir sehingga segala informasi yang disampaikan oleh klien akan dijaga kerahasiaannya</li>
                <li>Selama proses, memerlukan keterlibatan dan partisipasi aktif dari klien sehingga akan mengoptimalkan capaian yang diharapkan dari pembimbingan karir</li>
                <li>Pembimbingan karir dilakukan melalui sesi konseling dan konsultasi, dapat dilakukan secara individual maupun berkelompok sesuai dengan pertimbangan profesional dari Pembimbing Karir.</li>
                <li>Yang dimaksud dengan sesi individual adalah pertemuan yang dilakukan 4 mata antara klien dan Pembimbing Karir saja.</li>
                <li>Sedangkan sesi kelompok adalah layanan/bimbingan yang dilakukan secara bersama-sama , terdiri dari 6-8 orang mahasiswa dengan area permasalahan yang relatif serupa. Dalam sesi kelompok, klien bisa saling memberi dan memperoleh inspirasi dari rekan lain mengenai beragam alternatif solusi dan cara pandang.</li>
            </ol>
            <h2>Pembimbing Karir</h2>
            <ol>
                @foreach ($career_guidance_users as $item)
                    <li>{{$item->nama_lengkap}}</li>
                @endforeach
            </ol>
            <h5>Silahkan mengisi data berikut ini :
                (Setelah Mengisi Mohon konfirmasi ke 022-84288817, untuk kepastian jadwal konseling)</h5>
                <h6>Jika anda <b>alumni</b> silahkan untuk mendaftar terlebih dahulu <a href="{{url('register')}}">disini</a> </h6>
                @if ($msg = Session::get('success'))
                <div class="alert alert-success">
                    {{$msg}}
                </div>
                @endif
                @if ($msg = Session::get('error'))
                    <div class="alert alert-danger">
                        {{$msg}}
                    </div>
                @endif
                <div class="m-t-25">
                    <form action="{{route('bimbingan-karir.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>Nama Lengkap</b>  <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <input type="text" class="form-control m-b-15 @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" value="{{old('nama_lengkap')}}" placeholder="Nama Lengkap">
                            @error('nama_lengkap')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>NIM</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <input type="text" class="form-control m-b-15 @error('nim') is-invalid @enderror" name="nim" value="{{old('nim')}}" placeholder="NIM">
                            @error('nim')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>Program Studi</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <select name="program_studi" class="form-control m-b-15 @error('program_studi') is-invalid @enderror" id="">
                                <option value="Teknik Informatika">Teknik Informatika</option>
                                <option value="Sistem Informasi">Sistem Informasi</option>
                                <option value="Manajemen Informasi">Manajemen Informasi</option>
                            </select>
                            @error('program_studi')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>Pembimbing</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <select name="career_guidance_user_id" class="form-control m-b-15 @error('career_guidance_user_id') is-invalid @enderror" id="">
                                @foreach ($career_guidance_users as $item)
                                    <option value="{{$item->id}}">{{$item->nama_lengkap}}</option>
                                @endforeach
                            </select>
                            @error('career_guidance_user_id')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>Jenis Kelamin</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <div class="radio">
                                <input id="radio1" name="jenis_kelamin" value="Pria" type="radio" {{ old("jenis_kelamin") == 'Pria' ? 'checked' : '' }}>
                                <label for="radio1">Pria</label>
                            </div>
                            <div class="radio">
                                <input id="radio2" name="jenis_kelamin" value="Wanita" type="radio" {{ old("jenis_kelamin") == 'Wanita' ? 'checked' : '' }}>
                                <label for="radio2">Wanita</label>
                            </div>
                            @error('program_studi')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>Tempat Lahir</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <input type="text" class="form-control m-b-15 @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" value="{{old('tempat_lahir')}}" placeholder="Tempat Lahir">
                            @error('tempat_lahir')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>Tanggal Lahir</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <input type="date" class="form-control m-b-15 @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" value="{{old('tanggal_lahir')}}">
                            @error('tanggal_lahir')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>Email</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <input type="email" class="form-control m-b-15 @error('email') is-invalid @enderror" name="email" value="{{old('email')}}" placeholder="Email">
                            @error('email')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>Telepon</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <input type="number" class="form-control m-b-15 @error('telepon') is-invalid @enderror" name="telepon" value="{{old('telepon')}}" placeholder="Telepon">
                            @error('telepon')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>Area Permasalahan</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <textarea name="area_permasalahan" class="form-control m-b-15 @error('area_permasalahan') is-invalid @enderror" id="" cols="30" rows="10">{{old('area_permasalahan')}}</textarea>
                            @error('area_permasalahan')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>Kesediaan mengikuti sesi konsultasi secara individual maupun kelompok ?</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <div class="radio">
                                <input id="sesi1" name="sesi" type="radio" value="individual" {{ old("sesi") == 'individual' ? 'checked' : '' }}>
                                <label for="sesi1">Bersedia mengikuti sesi konsultasi individual saja</label>
                            </div>
                            <div class="radio">
                                <input id="sesi2" name="sesi" type="radio" value="kelompok" {{ old("sesi") == 'kelompok' ? 'checked' : '' }}>
                                <label for="sesi2">Bersedia mengikuti sesi konsultasi Kelompok saja</label>
                            </div>
                            <div class="radio">
                                <input id="sesi3" name="sesi" type="radio" value="keduanya" {{ old("sesi") == 'keduanya' ? 'checked' : '' }}>
                                <label for="sesi3">Bersedia mengikuti sesi konsultasi Individual maupun kelompok</label>
                            </div>
                            @error('sesi')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>Alasan Pilihan Kesediaan</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <input type="text" class="form-control m-b-15 @error('alasan_sesi') is-invalid @enderror" name="alasan_sesi" value="{{old('alasan_sesi')}}" placeholder="Alasan Sesi">
                            @error('alasan_sesi')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>Keterangan Diri Klien</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                                <?php $count = 1 ?>
                                @foreach ($self_descriptions as $item)
                                    <div class="checkbox">
                                        <input id="self_desc{{$count}}" type="checkbox" name="keterangan_diri[]" value="{{$item->id}}" @if(old('keterangan_diri', null) != null) {{ in_array($item->id, old("keterangan_diri")) ? 'checked' : '' }} @endif>
                                        <label for="self_desc{{$count}}">{{$item->keterangan}}</label>
                                    </div>
                                <?php $count++; ?>   
                                @endforeach
                            @error('keterangan_diri')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>Jadwal dan Pilihan Waktu</b><sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                                <?php $count = 1 ?>
                                @foreach ($career_guidance_schedules as $item)
                                    <div class="checkbox">
                                        <input id="jadwal{{$count}}" type="checkbox" name="jadwal[]" value="{{$item->id}}" @if(old('keterangan_diri', null) != null) {{ in_array($item->id, old("jadwal")) ? 'checked' : '' }} @endif>
                                        <label for="jadwal{{$count}}">{{$item->pembimbing->nama_lengkap}} | {{ucfirst($item->hari)}}, Pk {{date('H:i', strtotime($item->waktu_awal))}} - {{date('H:i', strtotime($item->waktu_selesai))}} WIB</label>
                                    </div>
                                <?php $count++; ?>   
                                @endforeach
                            @error('keterangan_diri')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><b>CV</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <input type="file" class="form-control m-b-15 @error('cv') is-invalid @enderror" name="cv">
                            <small>Max 2Mb, format : pdf,docx,pptx</small>
                            @error('cv')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-primary btn-md" title="Tambah Bimbingan Karir"><i class="anticon anticon-user-add"></i> Submit</button>
                        </div>
                    </div>
                </form>
                </div>
        </div>

    </div>
</section>

@endsection