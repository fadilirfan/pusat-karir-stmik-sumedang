<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Daftar</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/back/images/logo/favicon.png')}}">

    <!-- Core css -->
    <link href="{{asset('assets/back/css/app.min.css')}}" rel="stylesheet">
    {!! NoCaptcha::renderJs() !!}

</head>

<body>
    <div class="app">
        <div class="container-fluid p-h-0 p-v-20 bg full-height d-flex" style="background-image: url('assets/back/images/others/login-3.png')">
            <div class="d-flex flex-column justify-content-between w-100">
                <div class="container d-flex h-100">
                    <div class="row align-items-center w-100">
                        <div class="col-md-7 col-lg-5 m-h-auto">
                            <div class="card shadow-lg">
                                <div class="card-body">
                                    <div class="d-flex align-items-center justify-content-between m-b-30">
                                        <img class="img-fluid" alt="" src="assets/images/logo/logo.png">
                                        <h2 class="m-b-0">Daftar</h2>
                                    </div>
                                    <form method="POST" action="{{ route('register') }}">
                                        @csrf
                                        <div class="form-group">
                                            <label class="font-weight-semibold" for="fullname">Nama Lengkap:</label>
                                            <div class="input-affix">
                                                <i class="prefix-icon anticon anticon-contacts"></i>
                                                <input type="text"  class="form-control @error('fullname') is-invalid @enderror" name="fullname" value="{{ old('fullname') }}" id="fullname" placeholder="Nama Lengkap">
                                            </div>
                                            @error('fullname')
                                                <div class="row container">
                                                <span style="color:red">
                                                    {{$message}}
                                                </span>
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="font-weight-semibold" for="username">Username:</label>
                                            <div class="input-affix">
                                                <i class="prefix-icon anticon anticon-user"></i>
                                                <input type="text"  class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" id="username" placeholder="Username">
                                            </div>
                                            @error('username')
                                                <div class="row container">
                                                <span style="color:red">
                                                    {{$message}}
                                                </span>
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="font-weight-semibold" for="email">Email:</label>
                                            <div class="input-affix">
                                                <i class="prefix-icon anticon anticon-red-envelope"></i>
                                                <input type="text"  class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" id="email" placeholder="Username">
                                            </div>
                                            @error('email')
                                                <div class="row container">
                                                <span style="color:red">
                                                    {{$message}}
                                                </span>
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="font-weight-semibold" for="password">Password:</label>
                                            <div class="input-affix m-b-10">
                                                <i class="prefix-icon anticon anticon-lock"></i>
                                                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" id="password" placeholder="Password">
                                            </div>
                                            @error('password')
                                            <div class="row container">
                                                <span style="color:red">
                                                    {{$message}}
                                                </span>
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="font-weight-semibold" for="password_confirmation">Confirm Password:</label>
                                            <div class="input-affix m-b-10">
                                                <i class="prefix-icon anticon anticon-file-protect"></i>
                                                <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" value="{{ old('password_confirmation') }}" id="password_confirmation" placeholder="Konfirmasi Password">
                                            </div>
                                            @error('password_confirmation')
                                            <div class="row container">
                                                <span style="color:red">
                                                    {{$message}}
                                                </span>
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="font-weight-semibold">Captcha:</label>
                                            {!! app('captcha')->display() !!}
                                            @error('g-recaptcha-response')
                                            <div class="row container">
                                                <span style="color:red">
                                                    {{$message}}
                                                </span>
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <div class="d-flex align-items-center justify-content-between p-t-15">
                                                <button type="submit" class="btn btn-primary">Daftar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-none d-md-flex p-h-40 justify-content-center">
                    <span class="">© 2021 TAHUNGODING</span>
                </div>
            </div>
        </div>
    </div>

    
    <!-- Core Vendors JS -->
    <script src="{{asset('assets/back/js/vendors.min.js')}}"></script>

    <!-- page js -->
    <script src="{{asset('assets/back/js/pages/form-elements.js')}}"></script>

    <!-- Core JS -->
    <script src="{{asset('assets/back/js/app.min.js')}}"></script>

</body>

</html>
