@extends('back')
@section('title')
    Pengguna - Pusat Karir STMIK Sumedang
@endsection
@section('css')
<link href="{{asset('assets/back/vendors/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
@endsection
@section('js')
<script src="{{asset('assets/back/vendors/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/back/js/pages/dashboard-project.j')}}s"></script>
<script>
$('#data-table').DataTable();
</script>
@endsection
@section('content')
<div class="main-content">
    <div class="page-header">
        <h2 class="header-title">Pengguna</h2>
        <div class="header-sub-title">
            <nav class="breadcrumb breadcrumb-dash">
                <a href="#" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Dashboard</a>
                <span class="breadcrumb-item active">Pengguna</span>
            </nav>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <h4>Daftar Pengguna</h4>
            @if ($msg = Session::get('success'))
                <div class="alert alert-success">
                    {{$msg}}
                </div>
            @endif
            @if ($msg = Session::get('error'))
                <div class="alert alert-danger">
                    {{$msg}}
                </div>
            @endif
            <a href="{{route('users.create')}}" class="btn btn-primary"><i class="anticon anticon-user-add"></i> Tambah Data</a>
            <div class="m-t-25">
                <table id="data-table" class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>User Level</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $i)
                            <tr>
                                @php
                                    $path = 'input/users/profile/default.jpg';
                                    if (!empty($i->picture)) {
                                        $path = Storage::url($i->picture);
                                    }
                                @endphp
                                <td>
                                    <img src="{{$path}}" style="width:50px;height:50px;object-fit:cover;border-radius:30px;" alt="">
                                </td>
                                <td>{{$i->fullname}}</td>
                                <td>{{$i->username}}</td>
                                <td>{{$i->email}}</td>
                                <td>
                                    @if ($i->user_level->nama == 'Administrator')
                                        <span class="badge badge-success">{{$i->user_level->nama}}</span>
                                    @else 
                                        <span class="badge badge-secondary">{{$i->user_level->nama}}</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('users.edit', $i->id) }}" class="btn btn-rounded btn-info btn-xs"><i class="anticon anticon-edit"></i> Edit</a>
                                    <form action="{{ route('users.destroy', $i->id) }}" method="post"
                                        onsubmit="return confirm('Yakin hapus data ini?')">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-rounded btn-danger btn-xs" style="margin: 5px auto;">
                                            <i class="anticon anticon-delete"></i> Hapus</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection