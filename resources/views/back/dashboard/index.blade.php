@extends('back')
@section('title')
    Dashboard - Pusat Karir STMIK Sumedang
@endsection
@section('js')
<script src="{{asset('assets/back/vendors/chartjs/Chart.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/js/pages/dashboard-project.js')}}"></script>
@endsection
@section('css')
<link href="{{asset('assets/back/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="main-content">
        <div class="page-header no-gutters">
            <div class="d-md-flex align-items-md-center justify-content-between">
                <div class="media m-v-10 align-items-center">
                    <div class="avatar avatar-image avatar-lg">
                        @php
                            $profilePic = url('assets/back/images/avatars/thumb-3.jpg');
                            if (!empty(Auth::user()->picture)) {
                                $profilePic = Storage::url(Auth::user()->picture);
                            }
                        @endphp 
                        <img src="{{$profilePic}}" alt="">
                    </div>
                    <div class="media-body m-l-15">
                        <h4 class="m-b-0">Hai, {{Auth::user()->fullname}}!</h4>
                        <span class="text-gray">Civitas STMIK Sumedang</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection