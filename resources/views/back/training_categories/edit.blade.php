@extends('back')
@section('title')
    Ubah Pembimbing
@endsection
@section('css')
<link href="{{asset('assets/back/vendors/select2/select2.css')}}" rel="stylesheet">
<link href="{{asset('assets/back/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection
@section('js')
<script src="{{asset('assets/back/vendors/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/quill/quill.min.js')}}"></script>
<script src="{{asset('assets/back/js/pages/form-elements.js')}}"></script>
@endsection
@section('content')
<div class="main-content">
    <div class="page-header">
        <div class="header-sub-title">
            <nav class="breadcrumb breadcrumb-dash">
                <a href="dashboard" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Dashboard</a>
                <a class="breadcrumb-item" href="{{url('training_categories')}}">Pembimbing</a>
                <span class="breadcrumb-item active">Ubah</span>
            </nav>
        </div>
    </div>
    <div class="card col-md-4">
        <div class="card-body ">
            <h4>Ubah Pembimbing</h4>
            <div class="m-t-25">
                <form action="{{route('training_categories.update', $training_category->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Nama Kategori <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="text" class="form-control m-b-15 @error('nama') is-invalid @enderror" name="nama" value="{{ $training_category->nama ?? old('nama') }}" placeholder="Nama Kategori">
                        @error('nama')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Keterangan <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <textarea name="catatan" class="form-control m-b-15 @error('catatan') is-invalid @enderror" id="" cols="30" rows="10">{{ $training_category->catatan ?? old('catatan')}}</textarea>
                        @error('catatan')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary btn-md" title="Tambah Pembimbing"><i class="fas fa-plus-circle"></i> Submit</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ubahPassword">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ubahPasswordTitle">Ubah Password</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="anticon anticon-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                        @if ($msg = Session::get('success'))
                                    <div class="alert alert-success">
                                        {{$msg}}
                                    </div>
                            @endif
                            @if ($msg = Session::get('error'))
                                    <div class="alert alert-danger">
                                        {{$msg}}
                                    </div>
                            @endif
                    <form action="{{route('change_password', $training_category->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="col-md-12">
                        <label for="">Password Lama</label>
                        <input type="password" class="form-control m-b-15 @error('old_password') is-invalid @enderror" name="old_password"  placeholder="Password Lama">
                        @error('old_password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <label for="">Password Baru</label>
                        <input type="password" class="form-control m-b-15 @error('new_password') is-invalid @enderror" name="new_password"  placeholder="Password Baru">
                        @error('new_password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <label for="">Konfirmasi Password Baru</label>
                        <input type="password" class="form-control m-b-15 @error('confirm_password') is-invalid @enderror" name="confirm_password"  placeholder="Konfirmasi Password Baru">
                        @error('confirm_password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan perubahan</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection