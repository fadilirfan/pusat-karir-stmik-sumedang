@extends('back')
@section('title')
    Ubah Bimbingan Karir Alumni
@endsection
@section('css')
<link href="{{asset('assets/back/vendors/select2/select2.css')}}" rel="stylesheet">
<link href="{{asset('assets/back/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection
@section('js')
<script src="{{asset('assets/back/vendors/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/quill/quill.min.js')}}"></script>
<script src="{{asset('assets/back/js/pages/form-elements.js')}}"></script>
@endsection
@section('content')
<div class="main-content">
    <div class="page-header">
        <div class="header-sub-title">
            <nav class="breadcrumb breadcrumb-dash">
                <a href="dashboard" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Dashboard</a>
                <a class="breadcrumb-item" href="{{url('career_guidance_alumnis')}}">Bimbingan Karir Alumni</a>
                <span class="breadcrumb-item active">Ubah</span>
            </nav>
        </div>
    </div>
    <div class="card col-md-8">
        <div class="card-body ">
            <h4>Ubah Bimbingan Karir Alumni</h4>
            <div class="m-t-25">
                <form action="{{route('career_guidance_alumnis.update', $detail->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-12">
                        <label for=""><b>Nama Lengkap</b>  <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="text" class="form-control m-b-15 @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" value="{{ $detail->nama_lengkap ?? old('nama_lengkap')}}" placeholder="Nama Lengkap">
                        @error('nama_lengkap')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for=""><b>Program Studi</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <select name="program_studi" class="form-control m-b-15 @error('program_studi') is-invalid @enderror" id="">
                            <option value="Teknik Informatika" <?=($detail->program_studi == "Teknik Informatika") ? 'selected' : null;?>>Teknik Informatika</option>
                            <option value="Sistem Informasi" <?=($detail->program_studi == "Sistem Informasi") ? 'selected' : null;?>>Sistem Informasi</option>
                            <option value="Manajemen Informasi" <?=($detail->program_studi == "Manajemen Informasi") ? 'selected' : null;?>>Manajemen Informasi</option>
                        </select>
                        @error('program_studi')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for=""><b>Pembimbing</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <select name="career_guidance_user_id" class="form-control m-b-15 @error('career_guidance_user_id') is-invalid @enderror" id="">
                            @foreach ($career_guidance_users as $item)
                                <option value="{{$item->id}}" <?=($detail->career_guidance_user_id == $item->id) ? 'selected' : null;?>>{{$item->nama_lengkap}}</option>
                            @endforeach
                        </select>
                        @error('career_guidance_user_id')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for=""><b>Jenis Kelamin</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <div class="radio">
                            <input id="radio1" name="jenis_kelamin" value="Pria" type="radio" <?=($detail->jenis_kelamin == "Pria") ? 'checked' : null;?>>
                            <label for="radio1">Pria</label>
                        </div>
                        <div class="radio">
                            <input id="radio2" name="jenis_kelamin" value="Wanita" type="radio" <?=($detail->jenis_kelamin == "Wanita") ? 'checked' : null;?>>
                            <label for="radio2">Wanita</label>
                        </div>
                        @error('program_studi')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for=""><b>Tempat Lahir</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="text" class="form-control m-b-15 @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" value="{{ $detail->tempat_lahir ?? old('tempat_lahir')}}" placeholder="Tempat Lahir">
                        @error('tempat_lahir')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for=""><b>Tanggal Lahir</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="date" class="form-control m-b-15 @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" value="{{ $detail->tanggal_lahir ?? old('tanggal_lahir')}}">
                        @error('tanggal_lahir')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for=""><b>Email</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="email" class="form-control m-b-15 @error('email') is-invalid @enderror" name="email" value="{{ $detail->email ?? old('email')}}" placeholder="Email">
                        @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for=""><b>Telepon</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="number" class="form-control m-b-15 @error('telepon') is-invalid @enderror" name="telepon" value="{{ $detail->telepon ?? old('telepon')}}" placeholder="Telepon">
                        @error('telepon')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for=""><b>Area Permasalahan</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <textarea name="area_permasalahan" class="form-control m-b-15 @error('area_permasalahan') is-invalid @enderror" id="" cols="30" rows="10">{{ $detail->area_permasalahan ?? old('area_permasalahan')}}</textarea>
                        @error('area_permasalahan')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for=""><b>Kesediaan mengikuti sesi konsultasi secara individual maupun kelompok ?</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <div class="radio">
                            <input id="sesi1" name="sesi" type="radio" value="individual" <?=($detail->sesi == "individual") ? 'checked' : null;?>>
                            <label for="sesi1">Bersedia mengikuti sesi konsultasi individual saja</label>
                        </div>
                        <div class="radio">
                            <input id="sesi2" name="sesi" type="radio" value="kelompok" <?=($detail->sesi == "kelompok") ? 'checked' : null;?>>
                            <label for="sesi2">Bersedia mengikuti sesi konsultasi Kelompok saja</label>
                        </div>
                        <div class="radio">
                            <input id="sesi3" name="sesi" type="radio" value="keduanya" <?=($detail->sesi == "keduanya") ? 'checked' : null;?>>
                            <label for="sesi3">Bersedia mengikuti sesi konsultasi Individual maupun kelompok</label>
                        </div>
                        @error('sesi')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for=""><b>Alasan Pilihan Kesediaan</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <input type="text" class="form-control m-b-15 @error('alasan_sesi') is-invalid @enderror" name="alasan_sesi" value="{{ $detail->alasan_sesi ?? old('alasan_sesi')}}" placeholder="Alasan Sesi">
                        @error('alasan_sesi')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for=""><b>Keterangan Diri Klien</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <?php $count = 1 ?>
                            @foreach ($self_descriptions as $item)
                                <div class="checkbox">
                                    <input id="self_desc{{$count}}" type="checkbox" name="keterangan_diri[]" value="{{$item->id}}" <?=(in_array($item->id, $ex_self_desc)) ? 'checked' : null;?>>
                                    <label for="self_desc{{$count}}">{{$item->keterangan}}</label>
                                </div>
                            <?php $count++; ?>   
                            @endforeach
                        @error('keterangan_diri')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for=""><b>Jadwal dan Pilihan Waktu</b><sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                            <?php $count = 1 ?>
                            @foreach ($career_guidance_schedules as $item)
                                <div class="checkbox">
                                    <input id="jadwal{{$count}}" type="checkbox" name="jadwal[]" value="{{$item->id}}" <?=(in_array($item->id, $ex_jadwal)) ? 'checked' : null;?>>
                                    <label for="jadwal{{$count}}">{{$item->pembimbing->nama_lengkap}} | {{ucfirst($item->hari)}}, Pk {{date('H:i', strtotime($item->waktu_awal))}} - {{date('H:i', strtotime($item->waktu_selesai))}} WIB</label>
                                </div>
                            <?php $count++; ?>   
                            @endforeach
                        @error('keterangan_diri')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for=""><b>CV</b> <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        @if ($detail->cv)
                        <small>File cv sekarang : <a href="{{Storage::url($detail->cv)}}" download>Download</a></small>
                        @endif
                        <input type="file" class="form-control m-b-15 @error('cv') is-invalid @enderror" name="cv">
                        <small>Max 2Mb, format : pdf,docx,pptx</small>
                        @error('cv')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary btn-md" title="Ubah Bimbingan Karir Alumni"><i class="fas fa-plus-circle"></i> Submit</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection