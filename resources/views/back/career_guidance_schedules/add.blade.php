@extends('back')
@section('title')
    Tambah Jadwal Pembimbing
@endsection
@section('css')
<link href="{{asset('assets/back/vendors/select2/select2.css')}}" rel="stylesheet">
<link href="{{asset('assets/back/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/back/vendors/bootstrap-timepicker/bootstrap-timepicker.min.css')}}">
@endsection
@section('js')
<script src="{{asset('assets/back/vendors/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/quill/quill.min.js')}}"></script>
<script src="{{asset('assets/back/js/pages/form-elements.js')}}"></script>
<script src="{{asset('assets/back/vendors/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>
<script>
    $('.timepicker').timepicker({
        template: false,
        showInputs: false,
        defaultTime: 'current',
        showMeridian: false,
    });
</script>
@endsection
@section('content')
<div class="main-content">
    <div class="page-header">
        <div class="header-sub-title">
            <nav class="breadcrumb breadcrumb-dash">
                <a href="dashboard" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Dashboard</a>
                <a class="breadcrumb-item" href="{{url('career_guidance_schedules')}}">Jadwal Pembimbing</a>
                <span class="breadcrumb-item active">Tambah</span>
            </nav>
        </div>
    </div>
    <div class="card col-md-6">
        <div class="card-body ">
            <h4>Tambah Jadwal Pembimbing</h4>
            <div class="m-t-25">
                <form action="{{route('career_guidance_schedules.store')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Pembimbing <sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <select name="career_guidance_user_id" class="form-control m-b-15 @error('career_guidance_user_id') is-invalid @enderror" id="">
                            @foreach ($career_guidance_users as $item)
                                <option value="{{$item->id}}">{{$item->nama_lengkap}}</option>
                            @endforeach
                        </select>
                        @error('career_guidance_user_id')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Hari<sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <select name="hari" class="form-control m-b-15 @error('hari') is-invalid @enderror" id="">
                            <?php $hari = ['senin','selasa','rabu','kamis','jumat','sabtu'] ?>
                            <?php foreach ($hari as $key => $value) { ?>
                                <option value="{{$value}}">{{ucfirst($value)}}</option>
                            <?php } ?>
                        </select>
                        @error('hari')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Waktu Awal<sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <div class="input-group mb-3">
                            <input class="form-control timepicker" type="text" name="waktu_awal" />
                            <div class="input-group-append">
                             <span class="input-group-text"><i class="anticon anticon-clock-circle"></i></span>
                            </div>
                           </div>
                        @error('waktu_awal')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Waktu Selesai<sup title="Wajib diisi" style="color:red;cursor:pointer;">*</sup></label>
                        <div class="input-group mb-3">
                            <input class="form-control timepicker" type="text" name="waktu_selesai" />
                            <div class="input-group-append">
                             <span class="input-group-text"><i class="anticon anticon-clock-circle"></i></span>
                            </div>
                           </div>
                        @error('waktu_selesai')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Catatan</label>
                        <input type="text" name="catatan" class="form-control m-b-15 @error('catatan') is-invalid @enderror">
                        @error('catatan')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary btn-md" title="Tambah Pembimbing"><i class="fas fa-plus-circle"></i> Submit</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection