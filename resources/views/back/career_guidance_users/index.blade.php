@extends('back')
@section('title')
    Pembimbing - Pusat Karir STMIK Sumedang
@endsection
@section('css')
<link href="{{asset('assets/back/vendors/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
@endsection
@section('js')
<script src="{{asset('assets/back/vendors/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/back/js/pages/dashboard-project.j')}}s"></script>
<script>
$('#data-table').DataTable();
</script>
@endsection
@section('content')
<div class="main-content">
    <div class="page-header">
        <h2 class="header-title">Pembimbing</h2>
        <div class="header-sub-title">
            <nav class="breadcrumb breadcrumb-dash">
                <a href="#" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Dashboard</a>
                <span class="breadcrumb-item active">Pembimbing</span>
            </nav>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <h4>Daftar Pembimbing</h4>
            @if ($msg = Session::get('success'))
                <div class="alert alert-success">
                    {{$msg}}
                </div>
            @endif
            @if ($msg = Session::get('error'))
                <div class="alert alert-danger">
                    {{$msg}}
                </div>
            @endif
            <a href="career_guidance_users/create" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Tambah Data</a>
            <div class="m-t-25">
                <table id="data-table" class="table">
                    <thead>
                        <tr>
                            <th>NIDN</th>
                            <th>Nama</th>
                            <th>Bidang Ilmu</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($career_guidance_users as $i)
                            <tr>
                                @php
                                    $path = 'input/career_guidance_users/profile/default.jpg';
                                    if (!empty($i->picture)) {
                                        $path = Storage::url($i->picture);
                                    }
                                @endphp
                                <td>{{$i->nidn}}</td>
                                <td>{{$i->nama_lengkap}}</td>
                                <td>{{$i->bidang_ilmu}}</td>
                                <td>
                                    <a href="{{ route('career_guidance_users.edit', $i->id) }}" class="btn btn-rounded btn-info btn-xs"><i class="anticon anticon-edit"></i> Edit</a>
                                    <form action="{{ route('career_guidance_users.destroy', $i->id) }}" method="post"
                                        onsubmit="return confirm('Yakin hapus data ini?')">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-rounded btn-danger btn-xs" style="margin: 5px auto;">
                                            <i class="anticon anticon-delete"></i> Hapus</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection