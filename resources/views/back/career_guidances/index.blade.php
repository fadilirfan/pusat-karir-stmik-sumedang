@extends('back')
@section('title')
    Bimbingan Karir - Pusat Karir STMIK Sumedang
@endsection
@section('css')
<link href="{{asset('assets/back/vendors/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
@endsection
@section('js')
<script src="{{asset('assets/back/vendors/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/back/js/pages/dashboard-project.j')}}s"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script>
$(document).ready(function() {
    var table = $('#data-table').DataTable( {
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    } );
} );
</script>
@endsection
@section('content')
<div class="main-content">
    <div class="page-header">
        <h2 class="header-title">Bimbingan Karir</h2>
        <div class="header-sub-title">
            <nav class="breadcrumb breadcrumb-dash">
                <a href="#" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Dashboard</a>
                <span class="breadcrumb-item active">Bimbingan Karir</span>
            </nav>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <h4>Daftar Bimbingan Karir</h4>
            @if ($msg = Session::get('success'))
                <div class="alert alert-success">
                    {{$msg}}
                </div>
            @endif
            @if ($msg = Session::get('error'))
                <div class="alert alert-danger">
                    {{$msg}}
                </div>
            @endif
            <a href="career_guidances/create" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Tambah Data</a>
            <div class="m-t-25">
                <table id="data-table" class="table">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>NIM</th>
                            <th>Program Studi</th>
                            <th>Pembimbing</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($career_guidances as $i)
                            <tr>
                                <td>
                                    <a href="{{route('career_guidances.show', $i->id)}}">{{$i->nama_lengkap}}</a>    
                                </td>
                                <td>{{$i->nim}}</td>
                                <td>{{$i->program_studi}}</td>
                                <td>{{$i->pembimbing->nama_lengkap}}</td>
                                <td>
                                    @if ($i->status == 'Open')
                                        <span class="badge badge-success">Open</span>
                                    @else
                                        <span class="badge badge-danger">Close</span>
                                    @endif 
                                </td>
                                <td>
                                    @if ($i->status == 'Open')
                                    <a href="{{ route('career_guidances.edit', $i->id) }}" class="btn btn-rounded btn-info btn-xs mt-1" title="Edit"><i class="anticon anticon-edit"></i></a>
                                    <form action="{{ route('career_guidances.destroy', $i->id) }}" method="post"
                                        onsubmit="return confirm('Yakin hapus data ini?')">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-rounded btn-danger btn-xs" style="margin: 5px auto;">
                                            <i class="anticon anticon-delete" title="Hapus"></i></button>
                                    </form>
                                    @else 
                                    <span class="badge badge-secondary"><i>No Aksi</i> </span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection