@extends('back')
@section('title')
    Kategori Acara - Pusat Karir STMIK Sumedang
@endsection
@section('css')
<link href="{{asset('assets/back/vendors/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
@endsection
@section('js')
<script src="{{asset('assets/back/vendors/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/back/js/pages/dashboard-project.j')}}s"></script>
<script>
$('#data-table').DataTable();
</script>
@endsection
@section('content')
<div class="main-content">
    <div class="page-header">
        <h2 class="header-title">Kategori Acara</h2>
        <div class="header-sub-title">
            <nav class="breadcrumb breadcrumb-dash">
                <a href="#" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Dashboard</a>
                <span class="breadcrumb-item active">Kategori Acara</span>
            </nav>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <h4>Daftar Kategori Acara</h4>
            @if ($msg = Session::get('success'))
                <div class="alert alert-success">
                    {{$msg}}
                </div>
            @endif
            @if ($msg = Session::get('error'))
                <div class="alert alert-danger">
                    {{$msg}}
                </div>
            @endif
            <a href="event_categories/create" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Tambah Data</a>
            <div class="m-t-25">
                <table id="data-table" class="table">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($event_categories as $i)
                            <tr>
                                <td>{{$i->nama}}</td>
                                <td>{{$i->catatan}}</td>
                                <td>
                                    <a href="{{ route('event_categories.edit', $i->id) }}" class="btn btn-rounded btn-info btn-xs"><i class="anticon anticon-edit"></i> Edit</a>
                                    <form action="{{ route('event_categories.destroy', $i->id) }}" method="post"
                                        onsubmit="return confirm('Yakin hapus data ini?')">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-rounded btn-danger btn-xs" style="margin: 5px auto;">
                                            <i class="anticon anticon-delete"></i> Hapus</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection