@extends('layouts.app')
@section('title')
    Pelatihan
@endsection
@section('bg-image')
 style="background: url('assets/front/media/pelatihan.jpeg') no-repeat;width:100%;background-size:cover;"
@endsection
@section('content')
    <section class="white-bg">
        <div class="container">
            <div class="row justify-content-start blog-items-home"><!-- start row -->
                    @if ($trainings->isEmpty())
                        <h1>Belum ada data</h1>
                        <br><br>
                    @endif
                    @foreach ($trainings as $item)
                        <div class="col-md-4 "><!-- col -->
                            <div class="home-blog-te shadow p-3 mb-5 bg-body rounded"><!-- blog container -->
                                <div class="post-thumbn parallax-window" style="background: url({{url(Storage::url($item->thumbnail))}}) no-repeat center;"></div><!-- post thumbnail -->
                                <div class="post-bodyn">
                                    <h5><a href="{{route('pelatihan.show', $item->slug)}}">{{$item->judul}}</a></h5><!-- post title -->
                                    <p><i class="far fa-calendar"></i>{{date("F j, Y", strtotime($item->created_at))}}</p><!-- post date -->
                                </div>
                            </div><!-- end blog container -->
                        </div><!-- end col -->
                    @endforeach
                </div>	
            </div>
	</section>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            {{$trainings->links()}}
        </nav>
@endsection