@extends('layouts.app')
@section('title')
    {{$detail->judul}}
@endsection
@section('content')
<section class="padding-100-0 position-relative">
    <div class="body_overlay_ono"></div>
    <div class="container">
        <div class="col-md-12 help-center-header ">
            <h2 class="help-center-title">{{$detail->judul}}</h2>
            <small>Artikel ini dibuat <i>{{date("F j, Y H:i", strtotime($detail->created_at))}}</i>  oleh <b>{{$detail->author->fullname}}</b> </small>
            <p> 
                {!! $detail->deskripsi !!}
            </p>
            <small>Kategori: <a href="#">{{$detail->category->nama}}</a> </small>
        </div>

    </div>
</section>

@endsection